[![shields](https://img.shields.io/badge/project%20status-validated-deepgreen)](https://shields.io/)
[![shields](https://img.shields.io/badge/made%20with-java-orange)](https://shields.io/)
[![shields](https://img.shields.io/badge/powered%20by-spring-green)](https://shields.io/)
____________________


# Améliorez votre application avec des systèmes distribués

### Descrption du projet
TourGuide est une application qui permet aux utilisateurs de voir quelles sont les attractions touristiques à proximité et d’obtenir des réductions sur les séjours d’hôtel ainsi que sur les billets de différents spectacles. 

### Objectif du projet
Améliorer les perfomances, corriger des bus et ajouter des fonctionnalités.

### Prerequis pour installer et lancer les applications

- Java 1.8 JDK (or +)
- Gradle 6.6.1 (or +)
- Docker

### Run app
Se placer dans le repertoire racine de chanque projet et lancer l'application avec gradle

Gradle
```
gradle bootRun
```
### Recuperation des fichier .jar
- Aller dans la page du repository du projet
- Clicker sur la pastille verte de validation de la dernière pipeline.
- Clicker sur chacune des pipelines dans le job "build".
- Clicker sur "Download pour recuperer le .zip avec le fichier .jar .

### Installation des images docker
- Cloner le projet dans un repertoire git.
- A l'aide de Docker, ramèner les images crées
```
docker-compose pull
```
#### Déployer les images
```
docker-compose up -d
```
### Testing
Pour lancer les 2 types de test:
#### Test unitaires 
```
gradle test
```
#### Test d'integration 
```
gradle integrationTest
```

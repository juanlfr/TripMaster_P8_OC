package com.tripmaster.microservice.tourguide.UT;

import com.tripmaster.microservice.tourguide.beans.AttractionBean;
import com.tripmaster.microservice.tourguide.beans.LocationBean;
import com.tripmaster.microservice.tourguide.beans.VisitedLocationBean;
import com.tripmaster.microservice.tourguide.beans.user.User;
import com.tripmaster.microservice.tourguide.beans.user.UserReward;
import com.tripmaster.microservice.tourguide.helper.UnitTestHelper;
import com.tripmaster.microservice.tourguide.helpers.InternalTestHelper;
import com.tripmaster.microservice.tourguide.proxies.MicroserviceGpsProxy;
import com.tripmaster.microservice.tourguide.proxies.MicroserviceRewardsCentralProxy;
import com.tripmaster.microservice.tourguide.services.RewardsService;
import com.tripmaster.microservice.tourguide.services.TourGuideService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@SpringBootTest
public class RewardsServiceTest {
    @Mock
    private MicroserviceGpsProxy microserviceGpsProxy;
    @Mock
    private MicroserviceRewardsCentralProxy microserviceRewardsCentralProxy;
    @InjectMocks
    RewardsService rewardsService;
    @Autowired
    TourGuideService tourGuideService;

    private Double latitude;

    private Double longitude;

    private User user;


    @BeforeEach
    public void setUpTests() {
        Locale.setDefault(Locale.US);
        InternalTestHelper.internalUserMap.clear();
        user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        longitude = ThreadLocalRandom.current().nextDouble(-180.0, 180.0);
        longitude = Double.parseDouble(String.format("%.6f", longitude));
        latitude = ThreadLocalRandom.current().nextDouble(-85.05112878, 85.05112878);
        latitude = Double.parseDouble(String.format("%.6f", latitude));

    }

    @Test
    public void userGetRewards() throws ExecutionException, InterruptedException {


        Mockito.when(microserviceGpsProxy.getAttractions()).thenReturn(UnitTestHelper.getAttractions());
        AttractionBean attraction = UnitTestHelper.getAttractions().get(0);
        user.addToVisitedLocations(new VisitedLocationBean(user.getUserId(), attraction, new Date()));
        rewardsService.calculateRewards(user).get();
        List<UserReward> userRewards = user.getUserRewards();
        assertEquals(1, userRewards.size());
    }

    @Test
    public void isWithinAttractionProximity() {
        AttractionBean attraction = UnitTestHelper.getAttractions().get(0);
        assertTrue(rewardsService.isWithinAttractionProximity(attraction, attraction));
    }

    @Test
    public void nearAllAttractions() throws ExecutionException, InterruptedException {
        rewardsService.setProximityBuffer(Integer.MAX_VALUE);
        Mockito.when(microserviceGpsProxy.getAttractions()).thenReturn(UnitTestHelper.getAttractions());
        InternalTestHelper.generateUserLocationHistory(user);
        rewardsService.calculateRewards(user).get();
        assertEquals(UnitTestHelper.getAttractions().size(), user.getUserRewards().size());
    }

    @Test
    public void getDistanceTest() {
        LocationBean locationBean1 = new LocationBean(latitude, longitude);
        LocationBean locationBean2 = new LocationBean(latitude, longitude);

        Double distance = rewardsService.getDistance(locationBean1, locationBean2);

        assertNotNull(distance);

    }

}

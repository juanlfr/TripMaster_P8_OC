package com.tripmaster.microservice.tourguide.UT;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class ApplicationTests {

	@Autowired
	private Environment environement;

	@Test
	void contextLoads() {
		assertTrue(Arrays.stream(environement.getActiveProfiles()).anyMatch(profile -> profile.contains("test")));
	}

}

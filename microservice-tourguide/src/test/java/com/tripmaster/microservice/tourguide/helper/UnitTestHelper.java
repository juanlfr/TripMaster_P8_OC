package com.tripmaster.microservice.tourguide.helper;

import com.tripmaster.microservice.tourguide.beans.AttractionBean;
import com.tripmaster.microservice.tourguide.beans.Provider;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class UnitTestHelper {

    public static List<AttractionBean> getAttractions() {
        List<AttractionBean> attractions = new ArrayList<>();
        attractions.add(new AttractionBean("Disneyland", "Anaheim", "CA", 33.817595, -117.922008));
        attractions.add(new AttractionBean("Jackson Hole", "Jackson Hole", "WY", 43.582767, -110.821999));
        attractions.add(new AttractionBean("Mojave National Preserve", "Kelso", "CA", 35.141689, -115.510399));
        attractions.add(new AttractionBean("Joshua Tree National Park", "Joshua Tree National Park", "CA", 33.881866, -115.90065));
        attractions.add(new AttractionBean("Buffalo National River", "St Joe", "AR", 35.985512, -92.757652));
        attractions.add(new AttractionBean("Hot Springs National Park", "Hot Springs", "AR", 34.52153, -93.042267));
        attractions.add(new AttractionBean("Kartchner Caverns State Park", "Benson", "AZ", 31.837551, -110.347382));
        attractions.add(new AttractionBean("Legend Valley", "Thornville", "OH", 39.937778, -82.40667));
        attractions.add(new AttractionBean("Flowers Bakery of London", "Flowers Bakery of London", "KY", 37.131527, -84.07486));
        attractions.add(new AttractionBean("McKinley Tower", "Anchorage", "AK", 61.218887, -149.877502));
        attractions.add(new AttractionBean("Flatiron Building", "New York City", "NY", 40.741112, -73.989723));
        attractions.add(new AttractionBean("Fallingwater", "Mill Run", "PA", 39.906113, -79.468056));
        attractions.add(new AttractionBean("Union Station", "Washington D.C.", "CA", 38.897095, -77.006332));
        attractions.add(new AttractionBean("Roger Dean Stadium", "Jupiter", "FL", 26.890959, -80.116577));
        attractions.add(new AttractionBean("Texas Memorial Stadium", "Austin", "TX", 30.283682, -97.732536));
        attractions.add(new AttractionBean("Bryant-Denny Stadium", "Tuscaloosa", "AL", 33.208973, -87.550438));
        attractions.add(new AttractionBean("Tiger Stadium", "Baton Rouge", "LA", 30.412035, -91.183815));
        attractions.add(new AttractionBean("Neyland Stadium", "Knoxville", "TN", 35.955013, -83.925011));
        attractions.add(new AttractionBean("Kyle Field", "College Station", "TX", 30.61025, -96.339844));
        attractions.add(new AttractionBean("San Diego Zoo", "San Diego", "CA", 32.735317, -117.149048));
        attractions.add(new AttractionBean("Zoo Tampa at Lowry Park", "Tampa", "FL", 28.012804, -82.469269));
        attractions.add(new AttractionBean("Franklin Park Zoo", "Boston", "MA", 42.302601, -71.086731));
        attractions.add(new AttractionBean("El Paso Zoo", "El Paso", "TX", 31.769125, -106.44487));
        attractions.add(new AttractionBean("Kansas City Zoo", "Kansas City", "MO", 39.007504, -94.529625));
        attractions.add(new AttractionBean("Bronx Zoo", "Bronx", "NY", 40.852905, -73.872971));
        attractions.add(new AttractionBean("Cinderella Castle", "Orlando", "FL", 28.419411, -81.5812));
        return attractions;
    }

    public static List<Provider> getPrice(String apiKey, UUID attractionId, int adults, int children, int nightsStay, int rewardsPoints) {
        List<Provider> providers = new ArrayList();
        Set<String> providersUsed = new HashSet();

        try {
            TimeUnit.MILLISECONDS.sleep((long) ThreadLocalRandom.current().nextInt(1, 50));
        } catch (InterruptedException var16) {
        }

        for (int i = 0; i < 5; ++i) {
            int multiple = ThreadLocalRandom.current().nextInt(100, 700);
            double childrenDiscount = (double) (children / 3);
            double price = (double) (multiple * adults) + (double) multiple * childrenDiscount * (double) nightsStay + 0.99 - (double) rewardsPoints;
            if (price < 0.0) {
                price = 0.0;
            }

            String provider = "";

            do {
                provider = getProviderName(apiKey, adults);
            } while (providersUsed.contains(provider));

            providersUsed.add(provider);
            providers.add(new Provider(attractionId, provider, price));
        }

        return providers;
    }

    public static String getProviderName(String apiKey, int adults) {
        int multiple = ThreadLocalRandom.current().nextInt(1, 10);
        switch (multiple) {
            case 1:
                return "Holiday Travels";
            case 2:
                return "Enterprize Ventures Limited";
            case 3:
                return "Sunny Days";
            case 4:
                return "FlyAway Trips";
            case 5:
                return "United Partners Vacations";
            case 6:
                return "Dream Trips";
            case 7:
                return "Live Free";
            case 8:
                return "Dancing Waves Cruselines and Partners";
            case 9:
                return "AdventureCo";
            default:
                return "Cure-Your-Blues";
        }
    }
    public static int getAttractionRewardPoints(UUID attractionId, UUID userId) {
        try {
            TimeUnit.MILLISECONDS.sleep((long)ThreadLocalRandom.current().nextInt(1, 1000));
        } catch (InterruptedException var4) {
        }

        int randomInt = ThreadLocalRandom.current().nextInt(1, 1000);
        return randomInt;
    }
}

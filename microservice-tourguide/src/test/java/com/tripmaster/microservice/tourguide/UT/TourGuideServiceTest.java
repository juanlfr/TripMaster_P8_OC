package com.tripmaster.microservice.tourguide.UT;

import com.tripmaster.microservice.tourguide.beans.AttractionBean;
import com.tripmaster.microservice.tourguide.beans.LocationBean;
import com.tripmaster.microservice.tourguide.beans.Provider;
import com.tripmaster.microservice.tourguide.beans.VisitedLocationBean;
import com.tripmaster.microservice.tourguide.beans.user.User;
import com.tripmaster.microservice.tourguide.beans.user.UserPreferences;
import com.tripmaster.microservice.tourguide.dto.AttractionDTO;
import com.tripmaster.microservice.tourguide.dto.UserAllCurrentLocationsDTO;
import com.tripmaster.microservice.tourguide.helper.UnitTestHelper;
import com.tripmaster.microservice.tourguide.helpers.InternalTestHelper;
import com.tripmaster.microservice.tourguide.proxies.MicroserviceGpsProxy;
import com.tripmaster.microservice.tourguide.proxies.TripPricerProxy;
import com.tripmaster.microservice.tourguide.services.RewardsService;
import com.tripmaster.microservice.tourguide.services.TourGuideService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ActiveProfiles("test")
@SpringBootTest
public class TourGuideServiceTest {
    @Mock
    MicroserviceGpsProxy microserviceGpsProxy;

    @Mock
    RewardsService rewardsService;

    @Mock
    TripPricerProxy tripPricerProxy;

    @InjectMocks
    TourGuideService tourGuideService;


    private Double latitude;

    private Double longitude;

    private User user;


    @BeforeEach
    public void setUpTests() {
        Locale.setDefault(Locale.US);
        InternalTestHelper.internalUserMap.clear();
        user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        longitude = ThreadLocalRandom.current().nextDouble(-180.0, 180.0);
        longitude = Double.parseDouble(String.format("%.6f", longitude));
        latitude = ThreadLocalRandom.current().nextDouble(-85.05112878, 85.05112878);
        latitude = Double.parseDouble(String.format("%.6f", latitude));

    }


    @Test
    public void getUserLocation() throws ExecutionException, InterruptedException {

        VisitedLocationBean visitedLocationBean = new VisitedLocationBean(user.getUserId(), new LocationBean(latitude, longitude), new Date());

        Mockito.when(microserviceGpsProxy.getUserLocation(user.getUserId())).thenReturn(visitedLocationBean);

        VisitedLocationBean visitedLocation = tourGuideService.trackUserLocation(user);

        assertEquals(visitedLocation.userId, user.getUserId());
    }

    @Test
    public void addUser() {


        User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

        tourGuideService.addUser(user);
        tourGuideService.addUser(user2);

        User retrivedUser = tourGuideService.getUser(user.getUserName());
        User retrivedUser2 = tourGuideService.getUser(user2.getUserName());

        assertEquals(user, retrivedUser);
        assertEquals(user2, retrivedUser2);
    }

    @Test
    public void getAllUsers() {

        User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

        tourGuideService.addUser(user);
        tourGuideService.addUser(user2);

        List<User> allUsers = tourGuideService.getAllUsers();

        assertTrue(allUsers.contains(user));
        assertTrue(allUsers.contains(user2));
    }

    @Test
    public void trackUser() throws ExecutionException, InterruptedException {

        VisitedLocationBean visitedLocationBean = new VisitedLocationBean(user.getUserId(), new LocationBean(latitude, longitude), new Date());

        Mockito.when(microserviceGpsProxy.getUserLocation(user.getUserId())).thenReturn(visitedLocationBean);

        VisitedLocationBean visitedLocation = tourGuideService.trackUserLocation(user);

        assertEquals(user.getUserId(), visitedLocation.userId);

    }

    @Test
    public void getNearbyAttractions() throws ExecutionException, InterruptedException {

        VisitedLocationBean visitedLocationMock = new VisitedLocationBean(user.getUserId(), new LocationBean(latitude, longitude), new Date());

        Mockito.when(microserviceGpsProxy.getAttractions()).thenReturn(UnitTestHelper.getAttractions());
        Mockito.when(rewardsService.getRewardPoints(any(AttractionBean.class), any(User.class))).thenReturn(new Random().nextInt());
        Mockito.when(rewardsService.getDistance(any(LocationBean.class), any(LocationBean.class))).thenCallRealMethod();

        List<AttractionDTO> attractions = tourGuideService.getNearByAttractions(visitedLocationMock, user);

        assertEquals(5, attractions.size());
    }

    @Test
    public void getTripDeals() {

        UserPreferences userPreferences = new UserPreferences();
        userPreferences.setTicketQuantity(4);
        userPreferences.setNumberOfAdults(2);
        userPreferences.setNumberOfChildren(2);
        userPreferences.setTripDuration(3);
        user.setUserPreferences(userPreferences);

        List<Provider> providersMock = UnitTestHelper.getPrice("test-server-api-key", user.getUserId(), user.getUserPreferences().getNumberOfAdults(),
                user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), new Random().nextInt());

        Mockito.when(tripPricerProxy.getPrice(any(String.class), any(UUID.class), any(Integer.class),
                any(Integer.class), any(Integer.class), any(Integer.class))).thenReturn(providersMock);

        List<Provider> providers = tourGuideService.getTripDeals(user);

        assertEquals(5, providers.size());
    }

    @Test
    public void setUserPreferencesTest() {

        UserPreferences userPreferences = new UserPreferences();
        userPreferences.setTicketQuantity(4);
        userPreferences.setNumberOfAdults(2);
        userPreferences.setNumberOfChildren(2);
        userPreferences.setTripDuration(3);
        user.setUserPreferences(userPreferences);
        List<Provider> providersMock = UnitTestHelper.getPrice("test-server-api-key", user.getUserId(), user.getUserPreferences().getNumberOfAdults(),
                user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), new Random().nextInt());

        Mockito.when(tripPricerProxy.getPrice(any(String.class), any(UUID.class), any(Integer.class),
                any(Integer.class), any(Integer.class), any(Integer.class))).thenReturn(providersMock);

        List<Provider> providers = tourGuideService.getTripDeals(user);

        User user2 = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

        UserPreferences userPreferences2 = new UserPreferences();
        userPreferences2.setTicketQuantity(3);
        userPreferences2.setNumberOfAdults(2);
        userPreferences2.setNumberOfChildren(1);
        userPreferences2.setTripDuration(4);
        user2.setUserPreferences(userPreferences2);

        List<Provider> providersMock2 = UnitTestHelper.getPrice("test-server-api-key", user.getUserId(), user.getUserPreferences().getNumberOfAdults(),
                user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), new Random().nextInt());

        Mockito.when(tripPricerProxy.getPrice(any(String.class), any(UUID.class), any(Integer.class),
                any(Integer.class), any(Integer.class), any(Integer.class))).thenReturn(providersMock2);


        List<Provider> providers2 = tourGuideService.getTripDeals(user2);

        assertNotEquals(providers, providers2);


    }

    @Test
    public void getAllUsersCurrentLocationTest() {

        InternalTestHelper.setInternalUserNumber(10);
        InternalTestHelper.initializeInternalUsers();
        List<UserAllCurrentLocationsDTO> userAllCurrentLocationsDTOS = tourGuideService.getAllUsersCurrentLocation();
        assertEquals(10, userAllCurrentLocationsDTOS.size());

    }

}
